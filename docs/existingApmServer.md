---
title: Install APM on existing server
hide_title: true
---
### APM on existing server
This scenario will install ELK Stack and Elastic Beats on a **same instance** - *where all our microservices resides.*

Following steps will guide you to setup the entire APM stack.


- Pre requisite
  - Java JDK 8, you can find it under `credence-apm/resources`
  - Oracle Instant Client, you can find it under `credence-apm/resources`
- Download entire APM stack from [here](https://cred-if.s3.ap-south-1.amazonaws.com/apm/credence-apm-entire-stack.zip).
- Extract `credence-apm-entire-stack` zip file to suitable location
- Add your configurations to `templateConfig.json` file from above extracted zip file.
- Run below command to configure paths defined in `templateConfig.json` file.
    ```sh
    credcli template -f templateConfig.json
    ```
- Start APM stack by double clicking on `credence-apm-entire-stack/startAll.bat` file.