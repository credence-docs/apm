---
title: Introduction
hide_title: true
slug: /
---

## What is APM?
APM (Application Performance Management) also known as Application management system.

- It is an application which monitors the performance of all our application and databases.
- Observes whether your servers are behaving normally
- It monitors CPU/Memory usage of a server.
- Track and notify if any server goes down.
- Monitor network/database-query traffic
- Monitor Database operations

## Why should we even use APM? 

- We never get to know if any server gets down unless the client notices it
    - Here as APM continuously monitor the servers, if it goes down then it will mail us saying the server has gone down.
- To investigate an issue or a server failure, we ask client for the logs.
    - APM maintains all the server logs, so we can investigate here directly without reaching to the client
- Database becomes slow
    - APM can give you top 10 queries which are hogging up the database resources.
- Client has to monitor and manage the app using `pm2 status`
    - Now we can also check whether all our apps on client side are working properly or not.


### Resources

- [**https://openapm.io/**](https://openapm.io/)