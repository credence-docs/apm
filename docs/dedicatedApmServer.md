---
title: Dedicated APM server
hide_title: true
---
## Dedicated APM server

This scenario case consists installation on two servers.
### ELK Stack
> This is installed on a dedicated seperate instance.

- Pre requisite
  - Java JDK 8, [follow](./resources)
- Download ELK Stack from [here](https://cred-if.s3.ap-south-1.amazonaws.com/apm/credence-apm-elk-stack.zip).
- Extract `credence-apm-elk-stack` zip file to suitable location.
- Start ELK stack by double clicking on `credence-apm-elk-stack/startAll.bat` file.

### Elastic Beats
> This is installed on same instance where all our microservices resides.

- Pre requisite
    - Oracle Instant Client, [follow](./resources)
- Download Elastic Beats from [here](https://cred-if.s3.ap-south-1.amazonaws.com/apm/credence-apm-beats.zip).
- Extract `credence-apm-beats` zip file to suitable location.
- Edit `templateConfig.json` file with suitable configuration.
- Run below command to apply your configuration to elastic beats config files.
 ```bash
 credcli template -f templateConfig.json
 ```
- Start Beats stack by double clicking on `credence-apm-beats/startAll.bat` file.

The following scenario consists installation on two servers.

![Screenshot](./assets/IDealFunds_APM_AWS.png)

