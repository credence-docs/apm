---
title: Preface
hide_title: true
---

## Intro
Credence APM stack is built on top of the Elastic stack which includes following components :
- Elasticsearch
- Logstash
- Kibana
- Beats (Metricbeat, Heartbeat, Filebeat)

## Implementation scenarios

1. [APM on existing application server](./existingApmServer.md)
2. [Dedicated server only for APM](./dedicatedApmServer.md)
