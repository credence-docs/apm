---
title: Resources
hide_title: true
---

### Java JDK
- You can find the setup file of java under  `credence-apm/resources` folder which you'll download while following installation steps.

### Oracle Instant Client
- You'll find the zip file of oracle instant client in `credence-apm/resources` folder 
- which you'll download while following installation steps.
- After extracting the oracle instant client file on your system perform the following steps to setup the oracle instant client.
  - Extract `resources/instantclient_19_10.zip` to `C:/oracle/`
  - Add `C:/oracle/instantclient_19_10`  to the `PATH` environment variable
  - Restart any terminal windows
