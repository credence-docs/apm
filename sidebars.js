module.exports = {
  someSidebar: [
    "introduction",
    {
      type: "category",
      label: "Installation",
      collapsed: true,
      items: [
        "preface",
        {
          type: "category",
          label: "Scenarios",
          items: ["existingApmServer", "dedicatedApmServer"],
        },
      ],
    },
    "resources",
    "todo"
  ],
};
