module.exports = {
  title: 'APM-DOCS',
  tagline: 'The tagline of my site',
  url: 'https://credence-docs.gitlab.io/apm',
  baseUrl: '/apm/',
  onBrokenLinks: 'warn',
  onBrokenMarkdownLinks: 'warn',
  favicon: 'img/favicon.ico',
  organizationName: 'Credence Analytics', // Usually your GitHub org/user name.
  projectName: 'APM', // Usually your repo name.
  themeConfig: {
    sidebarCollapsible: false,
    hideableSidebar: true,
    navbar: {
      title: 'Credence APM Documentation',
     },
     footer: {
      style: "dark",
      copyright: `Copyright © ${new Date().getFullYear()} Credence Analytics.`,
    },

  },
  presets: [
    [
      '@docusaurus/preset-classic',
      {
        docs: {
          sidebarPath: require.resolve('./sidebars.js'),
          routeBasePath: "/",
        },
        theme: {
          customCss: require.resolve('./src/css/custom.css'),
        },
      },
    ],
  ],
};
